# Introduction

This book will provide in-depth documentation and usage for the [`symple`] and [`symple_core`] libraries, but for now it is just a placeholder.

[`symple`]: https://docs.rs/symple
[`symple_core`]: https://docs.rs/symple_core