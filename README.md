# The Symple Book

The source for [The Symple Book](https://symple-rs.gitlab.io/book/).

## Contributing

This book is published under the same license as the Symple library. Contributing is as simple as forking, editing the contents of the `src/` directory, and making a pull request.

This book is built using [`mdBook`]. To preview your changes locally, install Rust, then use cargo to install [`mdBook`]:

```
cargo install mdbook
mdbook build --open
mdbook test
```

## License

The Symple Book is distributed under the terms of the MIT License or the Apache License (Version 2.0) at your option.

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT), and [COPYRIGHT](COPYRIGHT) for details.

[`mdBook`]: https://rust-lang-nursery.github.io/mdBook/index.html
